Ce que vous devez faire :
​
Créer la structure HTML5 de votre maquette : dans un premier temps, ne faites pas de CSS, créez uniquement les éléments HTML avec les bonnes balises sémantiques
Créer les variables CSS générales : les couleurs, les polices (et optionnellement les padding/margin en rem)
Ajoutez des classes à vos éléments en respectant la convention BEM
Stylez vos éléments en utilisant des Flexbox (et optionnellement des CSS Grid)
Réalisez l’intégration complète d’une seule page
Partagez votre code sur Gitlab
​

Aller plus loin :
​
Réalisez l’ensemble de l’intégration de l’application
Ajoutez du JavaScript et de la manipulation du DOM